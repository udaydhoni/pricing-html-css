function changePrice() {
    let button = document.querySelector(".togglebutton")
    let masterPrice = document.getElementById("masterPrice");
    let basicPrice = document.getElementById("basicPrice");
    let professionalPrice = document.getElementById("professionalPrice");

    if (button.checked == true) {
        masterPrice.textContent = "$39.99"
        basicPrice.textContent = "$19.99"
        professionalPrice.textContent = "$24.99"
        
    } else {
        masterPrice.textContent = "$399.99"
        basicPrice.textContent = "$199.99"
        professionalPrice.textContent = "$249.99"
    }
}